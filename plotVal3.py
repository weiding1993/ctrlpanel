import lcm
import sys
sys.path.insert(0,'lcmtypes')
from ctrlpanel import pidData_t
import matplotlib.pyplot as plt
import numpy
from collections import deque

# Defining variables for ploting with a fixed size
#xData = [0] * 49
#yData = [0] * 49

# Enables interactive plotting in matplotlib
plt.ion()

ax = deque([0.0]*50)
ay = deque([0.0]*50)
axline, = plt.plot(ax)
ayline, = plt.plot(ay)
plt.ylim([-1.1,1.1])



# Defines the extreme limits fot the y-axis of the plot
MAX = 0
MIN = 0

# **Include dynamic plot of data by appending values**
def my_handler(channel,data):

    msg = pidData_t.decode(data)
	# print ("PID output is : %f"%(msg.pidOutput))
	# h1.set_xdata(numpy.append(h1.get_xdata(),msg.pidOutput))
	# h1.set_ydata(numpy.append(h1.get_ydata(),msg.timestamp))

# Update the min and max y-axis values
    MAX = max(ay)
    MIN = min(ay)
#    
#    if MAX <= Settle_Limit_Upper and MIN >= Settle_Limit_Lower
#        Settle_time = msg.timestamp
    if len(ax) < 50:
        ay.append(msg.pidOutput)
        ax.append(msg.timestamp)

    else:

        ax.pop()
        ay.pop()
    
        ay.appendleft(msg.pidOutput)
        ax.appendleft(msg.timestamp)
    
# Updates present plot
#    drawnow(plotFunction(MIN,MAX))
    axline.set_xdata(ax)
    ayline.set_ydata(ay)
    # plt.ylim([MIN MAX])
    plt.draw()

# Removes last element

if __name__=="__main__":
    lc = lcm.LCM()
    subscription = lc.subscribe("PIDDATA",my_handler)
    
    try:
        while True:
            lc.handle()
        
    except KeyboardInterrupt:
        pass



# Script to print Bode plot
from scipy import signal
import matplotlib.pyplot as plt
import math

f = [0.1,0.5,1,2,5,10,20]
gain = [1.0064,1.0261,1.0280,0.9898,0.9688,0.9466,0.7846]
phase = [0.011061,0.032698,0.055367,0.055029,0.033287,0.032180,0.010473]
gain_db = []
phase_rad = []
f_rad = []
for i in range(len(gain)):
    f_rad.append(2*math.pi*f[i])
    gain_db.append(20 * math.log(gain[i]))
    phase_rad.append(-phase[i] * 2 * math.pi * f[i])

plt.figure()
plt.subplot(211)
plt.title("Bode Plot for PID controller")
plt.grid(True,which='minor')
plt.xlabel("Frequency (rad/sec)")
plt.ylabel("Gain (db)")
plt.semilogx(f, gain_db, basex = 10)    # Bode magnitude plot
plt.subplot(212)
plt.grid(True,which='minor')
plt.yticks((-math.pi,-math.pi/2,0,math.pi/2,math.pi),(r"$-\pi$",r"$-\frac{\pi}{2}$","0",r"$\frac{\pi}{2}$",r"$\pi$"))
plt.xlabel("Frequency (rad/sec)")
plt.ylabel("Phase (rads)")
plt.semilogx(f, phase_rad, basex = 10)  # Bode phase plot
plt.show()
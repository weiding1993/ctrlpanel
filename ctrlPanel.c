
/**************************************************************
*
*	CtrlPanel Test
*
***************************************************************/

#include "./libBBB/libBBB.h"
#include "math.h"
#include "PID.h"
#include <math.h>

/* LIBRARIES NEEDED FOR LCM*/
#include <inttypes.h>
#include <lcm/lcm.h>
#include "lcmtypes/ctrlPanelData_t.h"  // ctrlPanel lcm type
#include "lcmtypes/pidData_t.h"  // PID lcm type

//Sensor Limit calibration
//MEASURE FOR YOUR PANEL
#define F1MIN 44
#define F1MAX 1740
#define F2MIN 44
#define F2MAX 1740
#define P1MIN 1 
#define P1MAX 1766
#define P2MIN 1
#define P2MAX 1766
#define P3MIN 1
#define P3MAX 1766
//set range of pot control
//YOU NEED TO DEFINE THESE
#define KPMAX 0
#define KIMAX 0
#define KDMAX 0
#define omega 3.1415926
#define PERIOD 4

struct Ctrlpanel_data{
  	int fader1;
  	int fader2;
  	int pots[3];
	int switches[3];
    	int motorSignal;
	float fader1_scaled;
	float fader2_scaled;
	float pots_scaled[3];
};

void InitializePanel(void){

 	/* Initiate Pin 66 Linked to Switch 1 */
    //INCLUDE OTHER INPUTS!
        initPin(66);
        setPinDirection(66, INPUT);

        initPin(67);
        setPinDirection(67, INPUT);
        
	initPin(69);
        setPinDirection(69, INPUT);
        

	/* Initiate Pin 45 that defines motor direction */
        initPin(45);
        setPinDirection(45, OUTPUT);
	
        /* Initiate A/D Converter */
        initADC();

        /* Initiate PWM for Servo 1 */
        initPWM(SV1);
        setPWMPeriod(SV1H, SV1, 100000);
        setPWMPolarity(SV1H, SV1, 0);
        setPWMDuty(SV1H, SV1, 0);
        setPWMOnOff(SV1H, SV1, ON);

}

/*******************************************    
*       Scaling functions to scale AD readings
*       0 to 1 for pots and -1 to 1 for faders
*******************************************/

float ScaleInt2PosFloat(int input, int min, int max){
    //scale readings to between 0 and 1
    //IMPLEMENT ME!
	float res;
	res = (((float)(input - min))/(max - min));
	if (res<0)
		return (0.0);
	else if (res>1)
		return (1.0);
	else
		return(res);		
}

float ScaleInt2Float(int input, int min, int max){
    //scale readings to between -1 and 1
    //IMPLEMENT ME!
	float res;
        res = (2*((float)(input - min))/((float)(max - min))) - 1.0;
        if (res<-1.0)
                return (-1.0);
        else if (res>1.0)
                return (1.0);
        else    
                return(res);

}

/*******************************************    
*       Read values of the sensors
*       IMPLEMENT THIS FIRST TO TEST 
*******************************************/

void ReadPanelValues(struct Ctrlpanel_data * ctrlpanel){
	int i;
	/* Read ADCs and give time after each reading to settle */
	ctrlpanel->fader1 = readADC(HELPNUM, A0);
	usleep(100);
	ctrlpanel->fader2 = readADC(HELPNUM, A1);
	usleep(100);
	ctrlpanel->pots[0] = readADC(HELPNUM, A2);
	usleep(100);
	ctrlpanel->pots[1] = readADC(HELPNUM, A3);
	usleep(100);
	ctrlpanel->pots[2] = readADC(HELPNUM, A4);
	usleep(100);
    //ADD OTHER POTENTIOMETERS

	/* Scale the readings */
     //UNCOMMENT WHEN READY TO IMPLEMENT
	ctrlpanel->fader1_scaled = -ScaleInt2Float(ctrlpanel->fader1,F1MIN, F1MAX);	
	ctrlpanel->fader2_scaled = -ScaleInt2Float(ctrlpanel->fader2,F1MIN, F1MAX);	
	for(i=0;i<3;i++)
		ctrlpanel->pots_scaled[i] = ScaleInt2PosFloat(ctrlpanel->pots[i],P1MIN, P1MAX);
    

	/* Read Switches position */
	ctrlpanel->switches[0] = getPinValue(66);
	ctrlpanel->switches[1] = getPinValue(67);
	ctrlpanel->switches[2] = getPinValue(69);
    //ADD ADDITIONAL SWITCHES 

}

void ReadPanelValues_loop(struct Ctrlpanel_data * ctrlpanel){
	
	/* Read ADCs and give time after each reading to settle */
	ctrlpanel->fader1 = readADC(HELPNUM, A0);
	usleep(100);
	ctrlpanel->fader1_scaled = - ScaleInt2Float(ctrlpanel->fader1,F1MIN, F1MAX);	
}


void PrintPanelValues(struct Ctrlpanel_data * ctrlpanel){

	// CHANGE TO PRINT ALL INPUTS AND EVENTUAL OUTPUTS YOU ALSO WANT TO CHECK
	printf("fader1: %4f\n",ctrlpanel->fader1_scaled);
  //	printf("fader1: %d\n",ctrlpanel->fader1);
  //	printf("fader2: %d\n",ctrlpanel->fader2);

       	printf("fader2: %4f\n",ctrlpanel->fader2_scaled);
  //	printf("Pots 1: %d\n",ctrlpanel->pots[0]);
	//printf("Pots 2: %d\n",ctrlpanel->pots[1]);
	//printf("Pots 3: %d\n",ctrlpanel->pots[2]);
        printf("Pots 1: %4f\n",ctrlpanel->pots_scaled[0]);
	printf("Pots 2: %4f\n",ctrlpanel->pots_scaled[1]);
	printf("Pots 3: %4f\n",ctrlpanel->pots_scaled[2]);

	printf("Switches 1: %4d\n",ctrlpanel->switches[0]);
	printf("Switches 2: %4d\n",ctrlpanel->switches[1]);
	printf("Switches 3: %4d\n",ctrlpanel->switches[2]);
	
	printf("---------------------\n");
}

/*******************************************    
*       
*       CONTROLLERS
*
*******************************************/
void SimpleFaderDriver(struct Ctrlpanel_data * ctrlpanel){
	
	/* Drive Fader1 motor with Fader 2 position */
	/* Position of Fader 2 is reflected in the speed of Fader 1 */
	/* Fader 2 in middle = Fader 1 does not move */
	/* Fader 2 left = Fader 1 moves to left up to limit */
	/* Fader 2 right = Fader 1 moves to right up to limit */
	  
	ctrlpanel->motorSignal = 200*(ctrlpanel->fader2-1700/2);
      	setPinValue(45, ctrlpanel->motorSignal > 0);
      	setPWMDuty(SV1H, SV1, abs(ctrlpanel->motorSignal));

}

void BangBang(struct Ctrlpanel_data * ctrlpanel){
    //IMPLEMENT ME!
  int sPos,aPos,error;
  static int thresh = 100;
	//Read each of the faders
	aPos = ctrlpanel->fader1;
	sPos = ctrlpanel->fader2;

	error = (sPos - aPos);
	if(error>thresh){
		setPinValue(45,1);
		setPWMDuty(SV1H, SV1, 50000);
	}

	else if(error<-thresh){
		setPinValue(45,0);
		setPWMDuty(SV1H, SV1, 50000);
	}
	else
		setPWMDuty(SV1H, SV1, 0);

}

void PID(PID_t *pid, struct Ctrlpanel_data * ctrlpanel){
    //IMPLEMENT ME!
	pid->pidInput=-(ctrlpanel->fader2_scaled - ctrlpanel->fader1_scaled);
	pid->pidSetpoint=(-ctrlpanel->fader2_scaled);

	if(!ctrlpanel->switches[0])
		pid->kp = ctrlpanel->pots_scaled[0];
	else
		pid->kp = 0;

	if(!ctrlpanel->switches[1])
		pid->ki = ctrlpanel->pots_scaled[1];
	else
		pid->ki = 0;

	if(!ctrlpanel->switches[2])
		pid->kd = ctrlpanel->pots_scaled[2];
	else
		pid->kd = 0;
	
	PID_Compute(pid);
	setPinValue(45,(pid->pidOutput)<0);
	float PWM_d = 100000.0*pid->pidOutput;
	setPWMDuty(SV1H,SV1, (int)abs(PWM_d));
	//	pid->pidOutput = PWM_d;
	pid->prevInput=pid->pidInput;
		

}

/* Tried to generate periodic signal based on timestamp
	But ended up using time based on control loop time

	This one is not used*/
void PID_sin(PID_t *pid, struct Ctrlpanel_data * ctrlpanel, float t){

    float step_signal = sin(omega*t);
    if(step_signal > 0) step_signal = 1.0;
    if(step_signal < 0) step_signal = -1.0; 

	pid->pidInput=-(step_signal - ctrlpanel->fader1_scaled);
	pid->pidSetpoint=(-ctrlpanel->fader2_scaled);

	if(!ctrlpanel->switches[0])
		pid->kp = ctrlpanel->pots_scaled[0];
	else
		pid->kp = 0;

	if(!ctrlpanel->switches[1])
		pid->ki = ctrlpanel->pots_scaled[1];
	else
		pid->ki = 0;

	if(!ctrlpanel->switches[2])
		pid->kd = ctrlpanel->pots_scaled[2];
	else
		pid->kd = 0;
	
	PID_Compute(pid);

	setPinValue(45,(pid->pidOutput)>0);
	float PWM_d = 100000.0*pid->pidOutput;
	setPWMDuty(SV1H,SV1, (int)abs(PWM_d));
	//	pid->pidOutput = PWM_d;
	pid->prevInput=pid->pidInput;
		
}

/*******************************************    
*       
*       LCM Message Preparation
*
*******************************************/

void CtrlPanelDataLCM(struct Ctrlpanel_data * ctrlpanel, const char *channel, lcm_t * lcm){

    ctrlPanelData_t msg = {};
        
	/* Prepare message to send over lcm channel*/
    msg.timestamp = utime_now();
    msg.fader1 = ctrlpanel->fader1;
	msg.fader2 = ctrlpanel->fader2;
        
	
	msg.pots[0] = ctrlpanel->pots[0];
    msg.pots[1] = ctrlpanel->pots[1];
    msg.pots[2] = ctrlpanel->pots[2];
    msg.switches[0] = ctrlpanel->switches[0];
    msg.switches[1] = ctrlpanel->switches[1];
    msg.switches[2] = ctrlpanel->switches[2];

    ctrlPanelData_t_publish(lcm, channel, &msg);

}

void PIDLCM(struct Ctrlpanel_data * ctrlpanel, PID_t *pid, const char *channel, lcm_t * lcm){
	
	pidData_t msg = {};
	
    /* Prepare message to send over lcm channel*/
    //IMPLEMENT ME!
    msg.timestamp = utime_now();
    msg.pidInput = pid->pidInput;
    msg.pidSetpoint = pid->pidSetpoint;

    msg.pidOutput = pid->pidOutput;
    msg.Kp = pid->kp;
    msg.Ki = pid->ki;
	msg.Kd	= pid->kd;

	pidData_t_publish(lcm, channel, &msg);

}



/*******************************************    
*       
*       MAIN FUNCTION
* 
*******************************************/
int main()
{

	struct Ctrlpanel_data ctrlpanel;
        
	// For Controllers
	int64_t thisTime, lastTime, timeDiff;
	int updateRate = 10000;

	// PID
	PID_t *pid;
	pid = PID_Init(0,0,0);

        // LCM
        const char *channel1 = "CTRL_PANEL_DATA";   // LCM channel to publish messages
        const char *channel2 = "PIDDATA";   // LCM channel to publish messages
        lcm_t * lcm = lcm_create(NULL);
        
        if(!lcm) {
            return 1;
        }

	// Panel Initialization
	InitializePanel();
	int iterator = 0;
	int numbers;
	ReadPanelValues(&ctrlpanel);

	/* Never ending loop */	
       	//ctrlpanel.fader2_scaled = 0.8;
	while(1)
	{
	  
		/* for periodic wave, call ReadPanelValues_loop(&ctrlpanel) to read only Fader 1 value*/
	  	//ReadPanelValues_loop(&ctrlpanel);
	    ReadPanelValues(&ctrlpanel);

	  	thisTime = utime_now();
        timeDiff = thisTime - lastTime;
        if(timeDiff >= updateRate){
		
		lastTime = thisTime;
       	PID(pid, &ctrlpanel);

		//BangBang(&ctrlpanel);
	    		
		/*squared wave*/
	    //if(iterator == (int)(PERIOD/2)) {
		//ctrlpanel.fader2 = 1570;
		//ctrlpanel.fader2_scaled = -ScaleInt2Float(ctrlpanel.fader2,F2MIN, F2MAX); 
		//}
		//if (iterator == PERIOD) {
		//ctrlpanel.fader2 = 213;
		//ctrlpanel.fader2_scaled = -ScaleInt2Float(ctrlpanel.fader2,F2MIN, F2MAX);
		// iterator = 0;
	     // }
	  
	  	/*Sinusoid wave*/
		// ctrlpanel.fader2_scaled = 0.8*sin(2.0*3.1415/(float)PERIOD*(float)iterator);
		// ctrlpanel.fader2 = (int)((-ctrlpanel.fader2_scaled + 1.0)*((float)(F2MAX-F2MIN))*0.5 + (float)F2MIN);
		
		// if (iterator == PERIOD) {
		// 	iterator = 0;
	 	// }
      
	  
		//PrintPanelValues(&ctrlpanel);
		
		// UNCOMMENT TO INCLUDE LCM MESSAGE
		CtrlPanelDataLCM(&ctrlpanel,channel1,lcm);	
		PIDLCM(&ctrlpanel,pid,channel2,lcm);
	      	iterator ++;

	
	  }
	}

	return 0;
}

import lcm
import sys
sys.path.insert(0,'lcmtypes')
from ctrlpanel import pidData_t
import matplotlib.pyplot as plt
import numpy
from collections import deque

# Enables interactive plotting in matplotlib
plt.ion()

# Defining variables for ploting with a fixed size
ax = deque([0.0]*200)
ay = deque([0.0]*200)
axline, = plt.plot(ax)
ayline, = plt.plot(ay)
plt.ylim([-5,5])

# Defines the extreme limits fot the y-axis of the plot
MAX = 0
MIN = 0

Time_t0 = 0

# Defining variables for settling time
ERROR_BAND = 20
Settle_time = 0
TS_pidOUT = 0.0
TS_SET = False
Out_count = 0

# **Include dynamic plot of data by appending values**
def my_handler(channel,data):
    msg = pidData_t.decode(data)
    global Time_t0
# Defining variables for settling time
    global ERROR_BAND
    global Settle_time
    global TS_pidOUT
    global TS_SET
    global Out_count

    # if Time_t0 > 0:
    #     Time_t0 = msg.timestamp
    
# Update the min and max y-axis values
#    MAX = max(ay[30:50])
#    MIN = min(ay[30:50])

# Determine the settling time
    Settle_Limit_Upper = 0.5 + msg.pidSetpoint * ERROR_BAND/100.0
    Settle_Limit_Lower = -0.5 - msg.pidSetpoint * ERROR_BAND/100.0
    print Settle_Limit_Upper
    print Settle_Limit_Lower
    print msg.pidOutput
    
    if msg.pidOutput < Settle_Limit_Upper and msg.pidOutput > Settle_Limit_Lower:
        print "Condition 1 clear"
        if not TS_SET :
            print "Condition 2 clear"
            Settle_time = msg.timestamp
            TS_pidOUT = msg.pidOutput
            TS_SET = True
        Out_count += 1
        print Out_count
        if Out_count >=50:
            print "Condition 3 clear"
            # plt.hold(True)
            plt.plot(Settle_time,TS_pidOUT,'ro')
            plt.show()
    else :
        Settle_time = 0
        TS_pidOUT = 0
        TS_SET = False
    
    
    if len(ax) < 50:
        ay.append(msg.pidOutput)
        ax.append(msg.timestamp)

    else:

        ax.pop()
        ay.pop()
        ay.appendleft(msg.pidOutput)
        ax.appendleft(msg.timestamp)
    
# Updates present plot
#    drawnow(plotFunction(MIN,MAX))
    axline.set_xdata(ax)
    ayline.set_ydata(ay)
    plt.draw()

# Removes last element

if __name__=="__main__":
    lc = lcm.LCM()
    subscription = lc.subscribe("PIDDATA",my_handler)

    try:
        while True:
            lc.handle()
    except KeyboardInterrupt:
        pass



import sys
import lcm
sys.path.insert(0,'lcmtypes')
from ctrlpanel import pidData_t
import matplotlib.pyplot as plt
#from exlcm import example_t

#if len(sys.argv) < 2:
#    sys.stderr.write("usage: read-log <logfile>\n")
#    sys.exit(1)

log = lcm.EventLog("lcmlog-2015-09-20.00-0.5HZ", "r")
pidOutData = []
timeData = []

for event in log:
    if event.channel == "PIDDATA":
        msg = pidData_t.decode(event.data)

        pidOutData.append(msg.pidOutput)
        timeData.append(msg.timestamp)

#for i in range(0, 10000):
#    plt.plot(timeData[i], pidOutData[i], ls="--", color="red")
#print max(pidOutData)
#print min(pidOutData)
#print max(timeData)
#print min(timeData)

plt.plot(timeData, pidOutData, linestyle="dotted", color="red")
plt.show()

#    if not Time_t0:
#        Time_t0 = msg.timestamp
#
#    # Determine the settling time
#    Settle_Limit_Upper = msg.pidInput + msg.pidInput * ERROR_BAND/100.0
#    Settle_Limit_Lower = msg.pidInput - msg.pidInput * ERROR_BAND/100.0
#    
#    if msg.pidOutput < Settle_Limit_Upper and msg.pidOutput > Settle_Limit_Lower:
#        if not TS_SET :
#            Settle_time = msg.timestamp
#            TS_pidOUT = msg.pidOutput
#            TS_SET = True
#        Out_count += 1
#        if Out_count >=50:
#            plt.plot(Settle_time,TS_pidOUT,'ro')
#    else :
#        Settle_time = 0
#        TS_pidOUT = 0
#        TS_SET = False
#
#    if len(ax) < 50:
#        ay.append(msg.pidOutput)
#        ax.append(msg.timestamp)
#
#    else:
#        ax.pop()
#        ay.pop()
#        ay.appendleft(msg.pidOutput)
#        ax.appendleft(msg.timestamp)
#    
#    # Updates present plot
#    #    drawnow(plotFunction(MIN,MAX))
#    axline.set_xdata(ax)
#    ayline.set_ydata(ay)
#    plt.draw()

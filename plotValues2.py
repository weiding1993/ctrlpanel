import lcm
import sys
sys.path.insert(0,'lcmtypes')
from ctrlpanel import pidData_t
import matplotlib.pyplot as plt
import numpy
from drawnow import drawnow
import time

fig = plt.figure()
pl = fig.add_subplot(111)

xData = [0.0] * 49
yData = [0.0] * 49
# pl.ylim(-2,2)
hi, = pl.plot(xData, yData)


# draw and show it
fig.canvas.draw()
plt.show(block=False)

def my_handler(channel,data):
    msg = pidData_t.decode(data)
    
    yData.append(msg.pidOutput)
    xData.append(msg.timestamp)
 
    # set the new data
    hi.set_ydata(yData)
    hi.set_xdata(xData)

# Removes last element
    xData.pop(0)
    yData.pop(0)
    fig.canvas.draw()

# loop to update the data
if __name__=="__main__":
    lc = lcm.LCM()
    subscription = lc.subscribe("PIDDATA",my_handler)
    try:
        while True: 
            lc.handle()
            # fig.canvas.draw()
            # time.sleep(0.01)

    except KeyboardInterrupt:
        pass
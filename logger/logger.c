// file: listener.c
//gcc -o logger logger.c `pkg-config --cflags --libs lcm` ../lcmtypes/ctrlPanelData_t.o

#include <stdio.h>
#include <inttypes.h>
#include <lcm/lcm.h>
#include "../lcmtypes/ctrlPanelData_t.h"
#include <time.h>

time_t rawtime;
struct tm* info;

char fName[100];
 FILE* f;
static void
my_handler(const lcm_recv_buf_t *rbuf, const char * channel, 
        const ctrlPanelData_t * msg, void * user)
{

    fprintf(f, "%" PRId64 ",%d, %d\n",msg->timestamp, msg->fader1, msg->fader2);
}

int main(int argc, char ** argv)
{

    time(&rawtime);
    info = localtime(&rawtime);
    strftime(fName, 50, "%d%H%M%S.txt", info);
    
    f = fopen(fName, "a");
    if(f == NULL) {
        perror("File open error! \n");
        exit(0);
    }

    lcm_t * lcm = lcm_create(NULL);
    if(!lcm)
        return 1;

    ctrlPanelData_t_subscribe(lcm, "CTRL_PANEL_DATA", &my_handler, NULL);

    while(1)
        lcm_handle(lcm);

    lcm_destroy(lcm);
    return 0;
}

clear
close

frequency = 0.1;

data = importdata('20HZ.txt');

Time_0 = data(1,1);
Time = data(:,1) - Time_0; %in msecs

Fader_Input = data(:,3);
Fader_output = data(:,2);

middle_value = mean(Fader_Input,'omitnan')
Fader_input_First = [];
Fader_output_First = [];
Time_first = [];

time_first_half_period = 6;
% get data of the first period
for i = 1 : time_first_half_period
    
    Fader_input_First = [Fader_input_First; Fader_Input(i)];
    Fader_output_First = [Fader_output_First; Fader_output(i)];
    Time_first = [Time_first; Time(i)];
    
end

 max_output = max(Fader_output);
 max_input = max(Fader_Input);
 
% Find time for first cross
for i = 2 : time_first_half_period

    if (Fader_input_First(i) >= middle_value & Fader_input_First(i-1) <= middle_value) | (Fader_input_First(i) <= middle_value & Fader_input_First(i-1) >= middle_value)
        Time_mid_input = Time_first(i);
    end
    
    if (Fader_output_First(i) >= middle_value & Fader_output_First(i-1) <= middle_value) | (Fader_output_First(i) <= middle_value & Fader_output_First(i-1) >= middle_value)
        Time_mid_output = Time_first(i);
    end
    
end

amp_ratio = max_output/max_input
Time_shift = Time_mid_output - Time_mid_input

plot(Fader_Input);
hold on 
plot(Fader_output, 'r')

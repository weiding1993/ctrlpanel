import sys
import lcm
sys.path.insert(0,'lcmtypes')
from ctrlpanel import ctrlPanelData_t
import matplotlib.pyplot as plt

Time_t0 = 0

# Defining variables for settling time
ERROR_BAND = 10
Settle_time = 0
TS_pidOUT = 0.0
TS_SET = False
Out_count = 0

log = lcm.EventLog("lcmlog-2015-09-21.00", "r")
pidOutData = []
timeData = []
pidInData = []

def Build_LogList():
   for event in log:
#    Create a list of all the logged values
        if event.channel == "CTRL_PANEL_DATA":
            msg = ctrlPanelData_t.decode(event.data)
            pidOutData.append(msg.fader2)
            timeData.append(msg.timestamp)
            pidInData.append(msg.fader1)


# Defining use of global variables for settling time
def Find_TS():
    for i in range(len(timeData)):
        global Time_t0
        global ERROR_BAND
        global Settle_time
        global TS_pidOUT
        global TS_SET
        global Out_count

        if Time_t0 > 0:
            Time_t0 = timeData[i]

    # Determine the settling time
        Settle_Limit_Upper =  pidInData[i] + pidInData[i] * ERROR_BAND/100.0
        Settle_Limit_Lower =  pidInData[i] - pidInData[i] * ERROR_BAND/100.0

        if pidOutData[i] < Settle_Limit_Upper and pidOutData[i] > Settle_Limit_Lower:
            if abs(pidInData[i-1] - pidInData[i]) < 10:
                if not TS_SET :
                    Settle_time = timeData[i]
                    TS_pidOUT = pidOutData[i]
                    TS_SET = True
                Out_count += 1

                if Out_count >=100:
                    plt.plot(Settle_time, TS_pidOUT, linestyle="None", marker="o", markersize=10, color="blue",label = "Setlling Point")
        else :
            Settle_time = 0
            TS_pidOUT = 0
            TS_SET = False
            Out_count = 0


Build_LogList()
Find_TS()
plt.plot(timeData, pidOutData, color="red", label="Fader 2")
plt.plot(timeData, pidInData, color="black", label = "Fader 1")
plt.legend(loc='upper right')
plt.xlabel("Time")
plt.ylabel("Fader Output")
plt.show()
